# Assigning string to a variable
a = "This is a string"
print (a)
# Declaring a list
L = [1, "a" , "string" , 1+2]
print (L )
L.append(6)
print(L)
L.pop()
print(L)
print (L[1] )
#3Tuples
tup = (1, "a", "string", 1+2)
print(tup)
print(tup[1])
#Iterations in Python
i = 1
while (i < 10):
    print(i)
    i += 1
#Iteration by for loop on string
s = "Hello World"
for i in s :
    print(i)
# Iteration by for loop on list
L = [1, 4, 5, 7, 8, 9]
for i in L:
    print(i)
#Iteration by for loop for range
for i in range(0, 10):
    print (i)
# python List
List = []
print(List)
List = [10, 20, 14]
print(List)
List = ["Geeks", "For", "Geeks"]
print(List[0])
print(List[1])
print(len(List))
List = [['Geeks', 'For'] , ['Geeks']]
print(List)
#Addition of element in to a list
List = []
List.append(1)
List.append(2)
List.append(3)
List.append(4)
print(List)
#or
List1 = []
for i in range(1, 7):
    List1.append(i)
print(List1)
List1.append((8,9))
print(List1)
List2 = ['For', 'Geeks']
List.append(List2)
print(List)
#Insert method
List = [1, 2, 3, 4]
print(List)
List.insert(3, 12)
List.insert(0, 'Geeks')
print(List)
#Extend method
List = [1, 2, 3, 4]
print(List)
List.extend([8, 'Geeks', 'Always'])
print(List)
#Accessing elements from the List
List = ["Geeks", "For", "Geeks"]
print(List[0])
print(List[1])
List = [['Geeks', 'For'] , ['Geeks']]
print(List[0])
print(List[1])
print(List[0][1])
print(List[1][0])

#Negative indexing

List = [1, 2, 'Geeks', 4, 'For', 6, 'Geeks']
print(List[-1])
print(List[-3])
#Removing Element
List = [1, 2, 3, 4, 5, 6,
        7, 8, 9, 10, 11, 12]
print(List)
List.remove(5)
List.remove(6)
print(List)
#or
for i in range(1, 5):
    List.remove(i)
print(List)
#remove using pop method
List = [1, 2, 3, 4, 5]
List.pop()
print(List)
List.pop(2)
print(List)
#Slicing of a list

# Python program to demonstrate
# Removal of elements in a List

# Creating a List
List = ['G', 'E', 'E', 'K', 'S', 'F',
        'O', 'R', 'G', 'E', 'E', 'K', 'S']
print(List)
Sliced_List = List[3:8]
print(Sliced_List)
Sliced_List = List[5:]
print(Sliced_List)
Sliced_List = List[:]
print(Sliced_List)
#Negative Index list slicing
List = ['G', 'E', 'E', 'K', 'S', 'F',
        'O', 'R', 'G', 'E', 'E', 'K', 'S']
Sliced_List = List[:-6]
print(Sliced_List)

Sliced_List = List[-6:-1]
print(Sliced_List)
Sliced_List = List[::-1]
print(Sliced_List)
#python tuples....
Tuple1 = ()
print(Tuple1)
Tuple1 = ('Geeks', 'For')
print(Tuple1)
list1 = [1, 2, 4, 5, 6]
print(tuple(list1))
Tuple1 = tuple('Geeks')
print(Tuple1)
#
Tuple1 = (5, 'Welcome', 7, 'Geeks')
print(Tuple1)
Tuple1 = (0, 1, 2, 3)
Tuple2 = ('python', 'geek')
Tuple3 = (Tuple1, Tuple2)
print(Tuple3)
Tuple1 = ('Geeks',) * 3
print(Tuple1)

Tuple1 = ('Geeks')
n = 5
for i in range(int(n)):
    Tuple1 = (Tuple1,)
    print(Tuple1)
#Acesing tuples
Tuple1 = tuple("Geeks")
print(Tuple1[1])
Tuple1 = ("Geeks", "For", "Geeks")
a, b, c = Tuple1
print(a)
print(b)
print(c)
# Concatenaton of tuples
Tuple1 = (0, 1, 2, 3)
Tuple2 = ('Geeks', 'For', 'Geeks')
Tuple3 = Tuple1 + Tuple2
print(Tuple1)
print(Tuple2)
print(Tuple3)
# Slicing of a Tuple

# Slicing of a Tuple
Tuple1 = tuple('GEEKSFORGEEKS')
print(Tuple1[1:])
print(Tuple1[::-1])
print(Tuple1[4:9])

# Creating a Set
set1 = set()
print(set1)
set1 = set("GeeksForGeeks")
print(set1)
#Dictionary
X = {1: 'Geeks', 2: 'For', 3: 'Geeks'}
print(X)
Y = {'Name': 'Geeks', 1: [1, 2, 3, 4]}
print(Y)
Dict = {}
print(Dict)
Dict = dict({1: 'Geeks', 2: 'For', 3:'Geeks'})
print(Dict)
Dict = dict([(1, 'Geeks'), (2, 'For'),(3, 'Geeks')])
print(Dict)

Dict = {1: 'Geeks', 2: 'For',
        3: {'A': 'Welcome', 'B': 'To', 'C': 'Geeks'}}

print(Dict)
#Adding elements to a Dictionary
Dict = {}
print(Dict)
Dict[0] = 'Geeks'
Dict[2] = 'For'
Dict[3] = 1
print(Dict)
Dict['Value_set'] = 2, 3, 4
print(Dict)
Dict[2] = 'Welcome'
print(Dict)
Dict[5] = {'Nested' :{'1' : 'Life', '2' : 'Geeks'}}
print(Dict)
#Acessing element from a dictionary
Dict = {1: 'Geeks', 'name': 'For', 3: 'Geeks'}
print(Dict[1])
print(Dict["name"])
print(Dict.get(3))
#Accessing element of a nested dictionary
Dict = {'Dict1': {1: 'Geeks'},
        'Dict2': {'Name': 'For'}}
print(Dict['Dict1'])
print(Dict['Dict2'])
print(Dict['Dict1'][1])
print(Dict['Dict2']['Name'])
#Removing Elements from Dictionary
Dict = { 5 : 'Welcome', 6 : 'To', 7 : 'Geeks',
        'A' : {1 : 'Geeks', 2 : 'For', 3 : 'Geeks'},
        'B' : {1 : 'Geeks', 2 : 'Life'}}
print(Dict)
del Dict[6]
print(Dict)
del Dict['A'][2]
print(Dict)
# Creating a Dictionary
Dict = {1: 'Geeks', 'name': 'For', 3: 'Geeks'}
pop_ele = Dict.pop(1)
print(str(Dict))
# Deleting entire Dictionary
Dict = {1: 'Geeks', 'name': 'For', 3: 'Geeks'}
Dict.clear()
print(Dict)
#######Dictionary program################
####1. str(dic) :- This method is used to return the string, denoting all the dictionary keys with their values.
#2. items() :- This method is used to return the list with all dictionary keys with values##
dic = { 'Name' : 'Nandini', 'Age' : 19 }
print (str(dic))
print (dic.items())

# len() and type()
dic = {'Name': 'Nandini', 'Age': 19, 'ID': 2541997}
li = [1, 3, 5, 6]
print(len(dic))
print(type(dic))
print(type(li))
# clear() and copy()

dic1 = {'Name': 'Nandini', 'Age': 19}
dic3 = {}
dic3 = dic1.copy()
print(dic3.items())
dic1.clear()
print(dic1.items())
